'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: {
        js: 'build/js/',
        css: 'build/less/',
        html: 'build/'
    },
    src: {
        js: 'src/js/main.js',
        less: 'src/less/main.less'
    },
    watch: {
        js: [
            'src/js/*.js',
            'src/js/**/*.js'
        ],
        less: [
            'src/less/*.less',
            'src/less/**/*.less'
        ],
        html: [
            'src/*.html',
        ]
    },
    clean: '../'
};

var config = {
    open: false,
    proxy: '127.0.0.1:8000'
};

// var config = {
//     server: {
//         baseDir: "./htdocs"
//     },
//     tunnel: true,
//     host: 'localhost',
//     port: 9000,
//     logPrefix: "dev"
// };

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        // .pipe(rigger())
        // .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        //.pipe(concat('all.js'))
        .pipe(gulp.dest(path.build.js))
        // .pipe(rename('all.min.js'))
        // .pipe(uglify())
        //.pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('less:build', function () {
    gulp.src(path.src.less)
        .pipe(less())
        .on('error', function(err){ console.log(err.message); })
        .pipe(prefixer({browsers: ['last 10 versions']}))
        // .pipe(cssmin())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'js:build',
    'less:build'
]);


gulp.task('watch', function(){
    watch(path.watch.html, function(event, cb) {
        reload();
        console.log('[ '+ new Date().getTime() + '] HTML changed. Browser reloaded');
    });
    watch(path.watch.less, function(event, cb) {
        gulp.start('less:build');
    });
    watch(path.watch.js, function(event, cb) {
        gulp.start('js:build');
    });
});


gulp.task('default', ['build', 'webserver', 'watch']);